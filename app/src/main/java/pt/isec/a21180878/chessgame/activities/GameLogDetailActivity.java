package pt.isec.a21180878.chessgame.activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.logic.Move;
import pt.isec.a21180878.chessgame.utils.Constants;
import pt.isec.a21180878.chessgame.utils.GameLog;
import pt.isec.a21180878.chessgame.utils.MyApplication;

public class GameLogDetailActivity extends Activity implements Constants{

    private TextView tvPlayer, tvDate, tvMoveSequence, tvWin;
    private ImageView ivGameMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_log_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        tvPlayer = findViewById(R.id.tvPlayer);
        ivGameMode = findViewById(R.id.iv_gameMode);
        tvDate = findViewById(R.id.tv_date);
        tvMoveSequence = findViewById(R.id.tvMoveSequence);
        tvWin = findViewById(R.id.tvWin);

        int g = getIntent().getIntExtra(GAME_LOG_DETAIL, -1);
        if(g != -1){
            GameLog gameLog = MyApplication.getGameLog(g);

            switch (gameLog.getGameMode()){
                case SINGLEPLAYER:
                    ivGameMode.setImageDrawable(getResources().getDrawable(R.drawable.ic_memory));
                    break;
                case MULTIPLAYER_OFFLINE:
                    ivGameMode.setImageDrawable(getResources().getDrawable(R.drawable.ic_group));
                    break;
                case MULTIPLAYER_ONLINE:
                    ivGameMode.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_wifi));
                    break;
            }

            tvPlayer.setText(gameLog.getPlayer1() + " vs " + gameLog.getPlayer2());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy");
            String time = simpleDateFormat.format(gameLog.getDate());
            tvDate.setText(time);

            tvWin.setTextSize(32);
            tvWin.setTypeface(tvWin.getTypeface(), Typeface.BOLD | Typeface.ITALIC);


            if(gameLog.getWin()){
                tvWin.setText("W");
                tvWin.setTextColor(Color.GREEN);
            }
            else {
                tvWin.setText("L");
                tvWin.setTextColor(Color.RED);
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (Move m: gameLog.getMoves()) {
                stringBuilder.append(m.toString());
            }
            tvMoveSequence.setText(stringBuilder);
        }
    }
}
