package pt.isec.a21180878.chessgame.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Filipe on 22-Dec-17.
 */

public class MyApplication extends Application {
    private static final String TAG = "MyApplication";
    private static final String gamesLogFile = "Gameslog.dat";

    // PREFS
    private static final String PREFS_PROFILE_NAME = "PREFS_PROFILE_NAME";
    private static final String PREFS_PROFILE_PHOTO = "PREFS_PROFILE_PHOTO";
    private static final String PREFS_PROFILE_NGAMES = "PREFS_PROFILE_NGAMES";
    private static final String PREFS_PROFILE_NMULTIPLAYERGAMES = "PREFS_PROFILE_NMULTIPLAYERGAMES";
    private static final String PREFS_PROFILE_NWINS = "PREFS_PROFILE_NWINS";
    private static final String PREFS_PROFILE_NDEFEATS = "PREFS_PROFILE_NDEFEATS";

    public static MyApplication mApplication;
    private Profile profile;
    private static ArrayList<GameLog> gamesLog;

    public MyApplication() {
        this.mApplication = this;
        this.gamesLog = new ArrayList<>();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.profile = readProfileFromSharedPreferences();
        loadGamesLog();
    }

    public static MyApplication getMyApplication() {
        return mApplication;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile p) {
        this.profile = p;
        writeProfileInSharedPreferences();
    }

    public Profile readProfileFromSharedPreferences() {
        Profile p = new Profile();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        p.setProfileName(prefs.getString(PREFS_PROFILE_NAME, null));
        p.setnGames(prefs.getInt(PREFS_PROFILE_NGAMES, -1));
        p.setnMultiplayerGames(prefs.getInt(PREFS_PROFILE_NMULTIPLAYERGAMES, -1));
        p.setnWins(prefs.getInt(PREFS_PROFILE_NWINS, -1));
        p.setnDefeats(prefs.getInt(PREFS_PROFILE_NDEFEATS, -1));

        p.setProfilePhotoPath(prefs.getString(PREFS_PROFILE_PHOTO, null));

        return p;
    }

    public void writeProfileInSharedPreferences(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.putString(PREFS_PROFILE_NAME, profile.getProfileName())
                .putInt(PREFS_PROFILE_NGAMES, profile.getnGames())
                .putInt(PREFS_PROFILE_NMULTIPLAYERGAMES, profile.getnMultiplayerGames())
                .putInt(PREFS_PROFILE_NWINS, profile.getnWins())
                .putInt(PREFS_PROFILE_NDEFEATS, profile.getnDefeats());

        if (profile.getProfilePhotoPath() != null)
            editor.putString(PREFS_PROFILE_PHOTO, profile.getProfilePhotoPath());

        editor.commit();
    }

    public void saveProfile(Profile newProfile) {
        profile.setProfileName(newProfile.getProfileName());
        writeProfileInSharedPreferences();
    }

    private void loadGamesLog(){
        mApplication.gamesLog = null;
        try {
            FileInputStream fileInputStream = mApplication.openFileInput(gamesLogFile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            ArrayList<GameLog> logs = (ArrayList<GameLog>) objectInputStream.readObject();
            mApplication.gamesLog = logs;
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (mApplication.gamesLog == null){
            gamesLog = new ArrayList<>();
        }
    }

    public static void saveGamesLog(){

        try {
            FileOutputStream fileOutputStream = mApplication.openFileOutput(gamesLogFile, MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(mApplication.gamesLog);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<GameLog> getGamesLog() {
        return gamesLog;
    }

    public static GameLog getGameLog(int i) {
        return gamesLog.get(i);
    }

    public static void addGameLog(GameLog g){
        gamesLog.add(g);
        saveGamesLog();
    }

    public static void deleteGameLog(int position) {
        gamesLog.remove(position);
        saveGamesLog();
    }

    public static void deleteGameLog() {
        gamesLog.clear();
        saveGamesLog();
    }
}
