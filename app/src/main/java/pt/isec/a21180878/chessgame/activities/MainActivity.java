package pt.isec.a21180878.chessgame.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.utils.MyApplication;
import pt.isec.a21180878.chessgame.utils.NewGameDialog;
import pt.isec.a21180878.chessgame.utils.NewProfileDialog;

public class MainActivity extends Activity {

    private static final int MULTIPLE_PERMISSION_REQUEST = 500;

    private Context context;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;

        tvTitle = findViewById(R.id.tvTitle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Bungee-Regular.ttf");
        tvTitle.setTypeface(typeface);
        tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD | Typeface.ITALIC);

        Button newGame = findViewById(R.id.bt_new_game);
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewGameDialog dialog = new NewGameDialog(MainActivity.this);
                dialog.show();
            }
        });

        Button gamesLog = findViewById(R.id.bt_games_log);
        gamesLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GamesLogActivity.class);
                startActivity(intent);
            }
        });

        Button profile = findViewById(R.id.bt_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileActivity.class);
                startActivity(intent);
            }
        });


        Button credits = findViewById(R.id.bt_credits);
        credits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CreditsActivity.class);
                startActivity(intent);
            }
        });


        // request permissions for android M
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }

        MyApplication mApplication = (MyApplication) getApplicationContext();
        if (mApplication.readProfileFromSharedPreferences().getProfileName() == null) {
            Log.e(">>>>>>>>>>>", "profile name is null");
            askForProfileName();
        }
    }

    private void askForProfileName() {
        NewProfileDialog dialog = new NewProfileDialog(MainActivity.this);
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAndRequestPermissions() {
        boolean cameraPermision = checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
        boolean writeExternalStoragePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
        boolean readEnternalStoragePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermision) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (readEnternalStoragePermission) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeExternalStoragePermission) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSION_REQUEST);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        switch (requestCode) {
            case MULTIPLE_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission Granted Successfully. Write working code here.
                    Toast.makeText(context, "Multiple Permission Granted Successfully.", Toast.LENGTH_LONG).show();

                } else {
                    //You did not accept the request can not use the functionality.
                    Toast.makeText(context, "Multiple Permission Granted denied.", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }
}
