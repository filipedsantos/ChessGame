package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 22-Nov-17.
 */

public class King extends Piece implements Serializable {
    private Position start;

    public King(boolean isWhite, Game game, Position pos) {
        super(isWhite, game, pos);
        this.start = pos;
    }

    @Override
    public char getPieceShortName() {
        return isWhite() ? 'K' : 'k';
    }

    @Override
    public ArrayList<Position> getAttackedPositions() {
        ArrayList<Position> attackedPositions = new ArrayList<>();
        for (int dir = 0; dir < 360; dir += 45) {
            Position p = getPosition().getAdjacentPosition(dir);
            if (getChessBoard().isValidPosition(p)) {
                attackedPositions.add(p);
            }
        }
        return attackedPositions;
    }

    @Override
    public ArrayList<Position> getMovePositions() {
//        ArrayList<Position> positions = getAttackedPositions();
//        HashSet<Position> invalid = isWhite() ? getGame().getPositionsControlledByWhites() : getGame().getPositionsControlledByBlacks();
//
//        Iterator<Position> iterator = positions.iterator();
//        while (iterator.hasNext()) {
//            Position p = iterator.next();
//            if (invalid.contains(p) || getChessBoard().isOccupied(p) && isSameColor(getChessBoard().getPiece(p)))
//                iterator.remove();
//        }
//        return positions;

        ArrayList<Position> positions = new ArrayList<>();
        for(int dir = 0; dir < 360; dir += 45) {
            Position p = getPosition().getAdjacentPosition(dir);
            while(getChessBoard().isValidPosition(p)) {
                if(getChessBoard().isOccupied(p)) {
                    if(!isSameColor(getChessBoard().getPiece(p))) {
                        positions.add(p);
                    }
                    break;
                }
                positions.add(p);
                p = p.getAdjacentPosition(dir);
            }
        }
        return positions;
    }

    public boolean canCastle(Rook rook) {
        if (hasMoved() || rook.hasMoved() || isWhite() != rook.isWhite()) {
            return false;
        }

        int direction = rook.getPosition().getColumn() > getPosition().getColumn() ? 90 : -90;

        ArrayList<Position> positions = new ArrayList<>();
        Position p = getPosition().getAdjacentPosition(direction);
        for (int i = 0; i < 2; i++) {
            positions.add(p);
            p = p.getAdjacentPosition(direction);
        }

        HashSet<Position> attacked = isWhite() ? getGame().getPositionsControlledByBlacks() : getGame().getPositionsControlledByWhites();

        for (Position pos : positions) {
            if (attacked.contains(pos) || getChessBoard().isOccupied(pos))
                return false;
        }

        return true;
    }

    private boolean hasMoved() {
        if (start == null) {
            return false;
        }
        return !start.equals(getPosition());
    }

    @Override
    public int getValue() {
        return 500;
    }
}
