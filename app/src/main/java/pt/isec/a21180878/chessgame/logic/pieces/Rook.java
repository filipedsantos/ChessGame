package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 22-Nov-17.
 */

public class Rook extends Piece implements Serializable {
    private Position start;

    public Rook(boolean isWhite, Game game, Position pos) {
        super(isWhite, game, pos);
        this.start = pos;
    }

    @Override
    public char getPieceShortName() {
        return isWhite() ? 'R' : 'r';
    }

    public boolean hasMoved() {
        if(start == null) {
            return false;
        }
        return !start.equals(getPosition());
    }

    @Override
    public ArrayList<Position> getAttackedPositions() {
        ArrayList<Position> positions = new ArrayList<>();
        for (int dir = 0; dir < 360; dir += 45) {
            Position p = getPosition().getAdjacentPosition(dir);
            while (getChessBoard().isValidPosition(p)){
                positions.add(p);
                if(getChessBoard().isOccupied(p))
                    break;
                p = p.getAdjacentPosition(dir);
            }
        }
        return positions;
    }

    @Override
    public ArrayList<Position> getMovePositions() {
        ArrayList<Position> positions = new ArrayList<>();
        for(int dir = 0; dir < 360; dir += 90) {
            Position p = getPosition().getAdjacentPosition(dir);
            while(getChessBoard().isValidPosition(p)) {
                if(getChessBoard().isOccupied(p)) {
                    if(!isSameColor(getChessBoard().getPiece(p)))
                        positions.add(p);
                    break;
                }
                positions.add(p);
                p = p.getAdjacentPosition(dir);
            }
        }
        return positions;
    }

    @Override
    public int getValue() {
        return 50;
    }
}
