package pt.isec.a21180878.chessgame.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pt.isec.a21180878.chessgame.R;

/**
 * Created by Filipe on 22-Dec-17.
 */

public class NewProfileDialog extends Dialog implements View.OnClickListener, Constants {
    private Activity activity;
    private EditText etProfileName;
    private Button btCancel, btOK;

    public NewProfileDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_new_profile);
        etProfileName = findViewById(R.id.et_profileName);
        btCancel = findViewById(R.id.bt_cancel);btCancel.setOnClickListener(this);
        btOK = findViewById(R.id.bt_ok);btOK.setOnClickListener(this);

        this.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Profile profile = new Profile(Constants.PROFILE);
                MyApplication mApplication = (MyApplication)activity.getApplicationContext();
                mApplication.setProfile(profile);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Profile profile = new Profile(Constants.PROFILE);
        MyApplication mApplication = (MyApplication)activity.getApplicationContext();
        switch (view.getId()) {
            case R.id.bt_cancel:
                mApplication.setProfile(profile);
                this.dismiss();
                break;
            case R.id.bt_ok:
                String name = etProfileName.getText().toString();
                if(name.length() >= 2){
                    profile.setProfileName(name);
                    mApplication.setProfile(profile);
                }
                this.dismiss();
                break;
            default:
                break;

        }
    }
}
