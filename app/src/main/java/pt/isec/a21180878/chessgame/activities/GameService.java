package pt.isec.a21180878.chessgame.activities;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import pt.isec.a21180878.chessgame.logic.Game;
import pt.isec.a21180878.chessgame.logic.Player;
import pt.isec.a21180878.chessgame.utils.Constants;
import pt.isec.a21180878.chessgame.utils.MyApplication;
import pt.isec.a21180878.chessgame.utils.Profile;

public class GameService extends Service implements Constants {

    private static final int PORT = 8899;
    // Binder given to clients
    private final GameServiceBinder gameServiceBinder = new GameServiceBinder();
    ObjectInputStream input;
    ObjectOutputStream output;

    class GameServiceBinder extends Binder {

        GameService getService() {
            return GameService.this;
        }
    }

    private boolean running = false;

    /*Thread task = new Thread(new Runnable() {
        @Override
        public void run() {
            while (running) {
                //Log.i("[GameService}", ".... running");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i("[GameService}", ".... stopThread()");
        }
    });*/

    private ServerSocket serverSocket = null;
    private Socket socketGame = null;
    private Profile p;
    private Messenger messageHandler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("[GAME_SERVICE]", "onStartCommand");
        Bundle extras = intent.getExtras();
        messageHandler = (Messenger) extras.get("MESSENGER");

        /*running = true;
        if (!task.isAlive())
            task.start();*/

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e("GameService", "onBind");

        return gameServiceBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("GameService", "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.e("GameService", "onRebind");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
        Log.e("GameService", "onDestroy1");
        try {
            commThread.interrupt();
            if (socketGame != null)
                socketGame.close();
            if (output != null)
                output.close();
            if (input != null)
                input.close();
            /*if (task.isAlive())
                task.join();*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        input = null;
        output = null;
        socketGame = null;
        Log.e("GameService", "onDestroy2");

    }

    /**
     * method for clients
     */

    public void client(final String strIP, final int Port) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("ChessGame", "Connecting to the server  " + strIP);
                    socketGame = new Socket(strIP, Port);

                    output = new ObjectOutputStream(socketGame.getOutputStream());
                    input = new ObjectInputStream(socketGame.getInputStream());

                    // write profile to server
                    MyApplication myApplication = (MyApplication) getApplicationContext();
                    Profile p = myApplication.getProfile();
                    output.writeObject(p);
                    output.flush();

                    //wait receiving game
                    Profile profile = (Profile) input.readObject();
                    sendMessage(MESSAGE_START_GAME, profile);


                } catch (Exception e) {
                    socketGame = null;
                }
                if (socketGame == null) {
                    return;
                }
                commThread.start();
            }
        });
        t.start();
    }

    public void server() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(PORT);
                    socketGame = serverSocket.accept();
                    serverSocket.close();
                    serverSocket = null;

                    output = new ObjectOutputStream(socketGame.getOutputStream());
                    input = new ObjectInputStream(socketGame.getInputStream());

                    //wait receiving profile
                    Profile pClient = (Profile) input.readObject();
                    sendMessage(MESSAGE_CREATE_PROFILE, pClient);

                    commThread.start();
                } catch (Exception e) {
                    e.printStackTrace();
                    socketGame = null;
                }
                sendMessage(MESSAGE_CLOSE_DIALOG, null);
            }
        });
        t.start();
    }

    Thread commThread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                Log.i("[ GAME-SERVICE]", "commThread started");
                while (!Thread.currentThread().isInterrupted()) {
                    Game game = (Game) input.readObject();
                    Log.i("[ GAME_SERVIE]", "r2: " + game.version);

                    sendMessage(MESSAGE_REFRESH_BOARD, game);


                }
            } catch (Exception e) {
                sendMessage(MESSAGE_CLOSE_CONNECTION, null);
                Log.e("[ GAME-SERVICE]", "error::: " + e.toString());
            }
        }
    });

    public void send(Game game) {
        final Game gameToSend = game;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    output.writeObject(gameToSend);
                    output.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void sendProfile(Profile profile) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    output.writeObject(profile);
                    output.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void startGame(Game game) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    output.writeObject(game);
                    output.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void sendMessage(int msg, Object o) {
        Message message = Message.obtain();
        switch (msg) {
            case MESSAGE_CLOSE_DIALOG:
                message.arg1 = MESSAGE_CLOSE_DIALOG;
                break;
            case MESSAGE_REFRESH_BOARD:
                message.arg1 = MESSAGE_REFRESH_BOARD;
                message.obj = (Game) o;;
                break;
            case MESSAGE_CLOSE_CONNECTION:
                message.arg1 = MESSAGE_CLOSE_CONNECTION;
                break;
            case MESSAGE_CREATE_PROFILE:
                message.arg1 = MESSAGE_CREATE_PROFILE;
                message.obj = (Profile) o;
                break;
            case MESSAGE_START_GAME:
                message.arg1 = MESSAGE_START_GAME;
                message.obj = (Profile) o;
                break;

        }
        try {
            messageHandler.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


}
