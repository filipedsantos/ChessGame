package pt.isec.a21180878.chessgame.logic.board;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Filipe on 29-Nov-17.
 */

public class Position implements Serializable {

    // Directions to check adjacent Positions
    public static final int N   = 0;
    public static final int NE  = 45;
    public static final int E   = 90;
    public static final int SE  = 135;
    public static final int S   = 180;
    public static final int SW  = 225;
    public static final int W   = 270;
    public static final int NW  = 315;


    private int line, column;

    public Position(int line, int column) {
        this.line = line;
        this.column = column;
    }

    public Position(String pos) {
        if( pos.length() != 2){
            throw  new IllegalArgumentException("[ERROR] IllegalArgumentException -- pos != 2");
        }

        this.column = pos.charAt(0) - 'a';
        this.line = pos.charAt(1) - '1';
    }

    public int getLine() {
        return line;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return line == position.line && column == position.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(line, column);
    }

    public Position getAdjacentPosition(int direction) {
        while (direction < 0) {
            direction += 360;
        }
        direction %= 360;

        switch (direction) {
            case N:
                return new Position(line + 1, column);
            case NE:
                return new Position(line + 1, column + 1);
            case E:
                return new Position(line, column + 1);
            case SE:
                return new Position(line - 1, column + 1);
            case S:
                return new Position(line - 1, column);
            case SW:
                return new Position(line - 1, column - 1);
            case W:
                return new Position(line, column - 1);
            case NW:
                return new Position(line + 1, column - 1);
            default:
                throw  new IllegalArgumentException("[ERROR] IllegalArgumentException");
        }
    }

    public String getChessPosition(){
        return  "" + (Character.toString((char) ('A' + getColumn()))) + (getLine() + 1);
    }

    @Override
    public String toString() {
        return "" + this.line + this.column;
    }


}
