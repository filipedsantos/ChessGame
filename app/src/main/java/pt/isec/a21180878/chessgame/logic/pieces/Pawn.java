package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 22-Nov-17.
 */

public class Pawn extends Piece implements Serializable {
    private Position start, ep;

    public Pawn(boolean isWhite, Game game, Position pos) {
        super(isWhite, game, pos);
        this.start = pos;
        ep = getPosition().getAdjacentPosition(isWhite ? 0 : 180);
    }

    @Override
    public char getPieceShortName() {
        return isWhite() ? 'P' : 'p';
    }

    private boolean hasMoved() {
        if(start == null)
            return false;
        return !start.equals(getPosition());
    }

    @Override
    public ArrayList<Position> getAttackedPositions() {
        ArrayList<Position> positions = new ArrayList<>(2);
        Position p = getPosition();
        Position p1, p2;
        if(this. isWhite()){
            p1 = new Position(p.getLine() + 1, p.getColumn() - 1);
            p2 = new Position(p1.getLine(), p.getColumn() + 1);
        } else {
            p1 = new Position(p.getLine() - 1, p.getColumn() + 1);
            p2 = new Position(p1.getLine(), p.getColumn() - 1);
        }

        if(getChessBoard().isValidPosition(p1)) {
            positions.add(p1);
        }
        if(getChessBoard().isValidPosition(p2)) {
            positions.add(p2);
        }
        return positions;
    }

    @Override
    public ArrayList<Position> getMovePositions() {
        // First let's deal with the diagonal squares.
        ArrayList<Position> positions = getAttackedPositions();
        Iterator<Position> iterator = positions.iterator();
        while(iterator.hasNext()) {
            Position pos = iterator.next();
            if(getChessBoard().isOccupied(pos)) {
                Piece p = getChessBoard().getPiece(pos);
                if(isSameColor(p))
                    iterator.remove();
            }
            else {
                // Being extra safe
                Position ep = getGame().getEnPassantLocation();
                if(ep == null || !ep.equals(pos) || isSameColor(getGame().getEnPassantPawn()))
                    iterator.remove();
            }
        }

        // Next come the squares in front.
        int dir = isWhite() ? 0 : 180;
        Position p1 = getPosition().getAdjacentPosition(dir);
        if(getChessBoard().isValidPosition(p1) && !getChessBoard().isOccupied(p1)) {
            positions.add(p1);
            Position p2 = p1.getAdjacentPosition(dir);
            if(!hasMoved() && !getChessBoard().isOccupied(p2)) {
                positions.add(p2);
            }
        }
         return positions;
    }

    @Override
    public int getValue() {
        return 10;
    }

}
