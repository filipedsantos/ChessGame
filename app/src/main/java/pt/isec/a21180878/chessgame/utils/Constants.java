package pt.isec.a21180878.chessgame.utils;

/**
 * Created by Filipe on 19-Dec-17.
 */

public interface Constants {
    String SAVEDGAME = "SAVEDGAME";
    String GAMEMODE = "GAMEMODE";
    String TYPEONLINEGAME = "TYPEONLINEGAME";
    String TIMEPLAYER1 = "TIMEPLAYER1";
    String TIMEPLAYER2 = "TIMEPLAYER2";
    String TIMERON = "TIMERON";

    String GAME_LOG_DETAIL = "GAME_LOG_DETAIL";
    String PROFILE = "Profile";


    String stringIP = "192.168.1.5";

    //GameModes
    int SINGLEPLAYER = 300;
    int MULTIPLAYER_OFFLINE = 301;
    int MULTIPLAYER_ONLINE = 302;
    // Players
    int PLAYER_ONE = 0;
    int PLAYER_TWO = 1;
    int PLAYER_AI = 3;

    int SERVER = 0;
    int CLIENT = 1;

    // Services Messages
    int MESSAGE_CLOSE_DIALOG = 1000;
    int MESSAGE_REFRESH_BOARD = 1001;
    int MESSAGE_CLOSE_CONNECTION = 1002;
    int MESSAGE_CREATE_PROFILE = 1003;
    int MESSAGE_START_GAME = 1004;
}