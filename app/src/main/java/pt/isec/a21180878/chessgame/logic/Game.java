package pt.isec.a21180878.chessgame.logic;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import pt.isec.a21180878.chessgame.logic.board.ChessBoard;
import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.pieces.Bishop;
import pt.isec.a21180878.chessgame.logic.pieces.King;
import pt.isec.a21180878.chessgame.logic.pieces.Knight;
import pt.isec.a21180878.chessgame.logic.pieces.Pawn;
import pt.isec.a21180878.chessgame.logic.pieces.Piece;
import pt.isec.a21180878.chessgame.logic.pieces.Queen;
import pt.isec.a21180878.chessgame.logic.pieces.Rook;
import pt.isec.a21180878.chessgame.utils.Constants;
import pt.isec.a21180878.chessgame.utils.Profile;

public class Game implements Serializable, Constants {

    public int version = 0;

    // Pieces and grid information
    public ArrayList<Piece> pieces;
    private ChessBoard chessBoard;
    private HashSet<Position> whitePositions, blackPositions;

    // HashSet to verify InCheck King
    private HashSet<Move> escapeMoves;

    // Move and turn
    //public boolean whiteTurn;
    private ArrayList<Move> moveSequence;

    // Information related to en passant
    private Position ep_capture;
    private Pawn ep_pawn;
    // Castling Information
    private King wKing, bKing;

    private int gameMode;
    private Player[] player = new Player[2];
    private boolean isPlayerOne;

    private boolean gameRunning = false;
    private int winnerPlayer;
    private int looserPlayer;

    private boolean timerOn = false;
    int time[] = {0, 0};


    // Construtor
    public Game(int gameMode) {
        this.gameMode = gameMode;
        this.gameRunning = true;
        this.isPlayerOne = true;
        this.chessBoard = new ChessBoard();
        this.blackPositions = new HashSet<>(32);
        this.whitePositions = new HashSet<>(32);
        this.moveSequence = new ArrayList<>();
        createPieces();
        updatePositions();
        for (Piece p : pieces) {
            p.update();
        }

        player[PLAYER_ONE] = new Player();
        player[PLAYER_TWO] = new Player();

    }

    public Game(Game game) {
        this.gameMode = game.getGameMode();
        this.gameRunning = true;
        this.isPlayerOne = true;
        this.chessBoard = new ChessBoard();
        this.blackPositions = new HashSet<>(32);
        this.whitePositions = new HashSet<>(32);
        this.moveSequence = new ArrayList<>();
        createPieces();
        updatePositions();
        for (Piece p : pieces) {
            p.update();
        }

        player[PLAYER_ONE] = game.getPlayer(PLAYER_ONE);
        player[PLAYER_TWO] = game.getPlayer(PLAYER_TWO);
    }

    private void createPieces() {
        pieces = new ArrayList<>(32);
        // Kings
        bKing = new King(false, this, new Position("e8"));
        wKing = new King(true, this, new Position("e1"));
        pieces.add(bKing);
        pieces.add(wKing);
        // Queen
        Queen bQueen, wQueen;
        bQueen = new Queen(false, this, new Position("d8"));
        wQueen = new Queen(true, this, new Position("d1"));
        pieces.add(bQueen);
        pieces.add(wQueen);
        // Bishop
        Bishop bBishop1, bBishop2, wBishop1, wBishop2;
        bBishop1 = new Bishop(false, this, new Position("c8"));
        bBishop2 = new Bishop(false, this, new Position("f8"));
        wBishop1 = new Bishop(true, this, new Position("c1"));
        wBishop2 = new Bishop(true, this, new Position("f1"));
        pieces.add(bBishop1);
        pieces.add(bBishop2);
        pieces.add(wBishop1);
        pieces.add(wBishop2);
        // Knight
        Knight bKnight1, bKnight2, wKnight1, wKnight2;
        bKnight1 = new Knight(false, this, new Position("b8"));
        bKnight2 = new Knight(false, this, new Position("g8"));
        wKnight1 = new Knight(true, this, new Position("b1"));
        wKnight2 = new Knight(true, this, new Position("g1"));
        pieces.add(bKnight1);
        pieces.add(bKnight2);
        pieces.add(wKnight1);
        pieces.add(wKnight2);
        // Rooks
        Rook bRook1, bRook2, wRook1, wRook2;
        bRook1 = new Rook(false, this, new Position("a8"));
        bRook2 = new Rook(false, this, new Position("h8"));
        wRook1 = new Rook(true, this, new Position("a1"));
        wRook2 = new Rook(true, this, new Position("h1"));
        pieces.add(bRook1);
        pieces.add(bRook2);
        pieces.add(wRook1);
        pieces.add(wRook2);
        // Pawns
        for (int i = 0; i < 8; i++) {
            Pawn bp = new Pawn(false, this, new Position("" + (char) ('a' + i) + "7"));
            Pawn wp = new Pawn(true, this, new Position("" + (char) ('a' + i) + "2"));
            pieces.add(bp);
            pieces.add(wp);
        }

        // Add pieces to ChessBoard
        for (Piece p : pieces) {
            chessBoard.putPiece(p.getPosition(), p);
        }
    }

    private void updateAllowedMoves() {
        HashSet<Position> enemy = isPlayerOne() ? blackPositions : whitePositions;
        King our_king = isPlayerOne() ? wKing : bKing;
        ArrayList<Piece> all_pieces = new ArrayList<>(pieces.size());
        for (Piece p : pieces) {
            all_pieces.add(p);
        }

        for (Piece piece : all_pieces) {
            if ((isPlayerOne() != piece.isWhite()) || piece instanceof King)
                continue;

            Position prev = piece.getPosition();
            Iterator<Position> i = piece.getMoves().iterator();
            while (i.hasNext()) {
                Position pos = i.next();
                Piece captured = chessBoard.putPiece(pos, piece);
                if (captured != null)
                    pieces.remove(captured);
                //updatePositions();
                if (enemy.contains(our_king.getPosition())) {
                    System.out.println("Removing position " + pos + " for " + piece);
                    i.remove();
                }
                chessBoard.putPiece(prev, piece);
                if (captured != null) {
                    chessBoard.putPiece(pos, captured);
                    pieces.add(captured);
                } else
                    chessBoard.remove(pos);
                updatePositions();
            }
        }
    }

    private HashSet<Move> getEscapeMoves() {
        HashSet<Move> moves = new HashSet<>(3);
        ArrayList<Piece> all_pieces = new ArrayList<>();
        all_pieces.addAll(pieces);

        for (Piece p : all_pieces) {

            if (p.isWhite() != isPlayerOne())
                continue;

            Position prev = p.getPosition();
            for (Position pos : p.getMoves()) {
                Piece captured = chessBoard.putPiece(pos, p);
                if (captured != null)
                    pieces.remove(captured);
                updatePositions();
                if (!isInCheck())
                    moves.add(new Move(prev, pos));
                chessBoard.putPiece(prev, p);
                if (captured != null) {
                    chessBoard.putPiece(pos, captured);
                    pieces.add(captured);
                } else
                    chessBoard.remove(pos);
                updatePositions();
            }
        }
        return moves;
    }

    public boolean isInCheck() {
        return isPlayerOne() ? getPositionsControlledByBlacks().contains(wKing.getPosition()) : getPositionsControlledByWhites().contains(bKing.getPosition());
    }

    public boolean isCheckmate() {
        return isInCheck() && getEscapeMoves().size() < 2;
    }

    public int getGameMode() {
        return gameMode;
    }

    public HashSet<Position> getPositionsControlledByWhites() {
        return whitePositions;
    }

    public HashSet<Position> getPositionsControlledByBlacks() {
        return blackPositions;
    }

    public void updatePositions() {
        whitePositions.clear();
        blackPositions.clear();
        for (Piece p : pieces) {
            if (p.isWhite())
                whitePositions.addAll(p.getAttackedPositions());
            else
                blackPositions.addAll(p.getAttackedPositions());
        }
    }

    public Position getEnPassantLocation() {
        return ep_capture;
    }

    public Piece getEnPassantPawn() {
        return ep_pawn;
    }

    public int getActivePlayer() {
        if (isPlayerOne) {
            return PLAYER_ONE;
        } else {
            return PLAYER_TWO;
        }
    }

    public int getWinnerPlayer() {
        if (isPlayerOne) {
            return PLAYER_TWO;
        } else {
            return PLAYER_ONE;
        }
    }

    public Player getPlayer(int _player) {
        return player[_player];
    }

    public void setPlayer(int p, Profile profile) {
        player[p].setProfile(profile);
    }

    public boolean isPlayerOne() {
        return isPlayerOne;
    }

    public void swapActivePlayer() {
        isPlayerOne = !isPlayerOne;
    }

    public boolean move(Position src, Position dest) throws Exception {
        if (!chessBoard.isValidPosition(src) || !chessBoard.isValidPosition(dest)) {
            return false;
        }

        Piece piece = chessBoard.getPiece(src);
        if (piece instanceof Pawn && dest.getLine() == (piece.isWhite() ? 7 : 0)) {
            // Promote a Pawn
            return move(src, dest, 'Q');
        }
        if (piece instanceof King && !piece.getAttackedPositions().contains(dest)) {
            King king = (King) piece;
            Rook rook;
            Piece temp;
            if (dest.equals(new Position("g1")))
                temp = chessBoard.getPiece(new Position("h1"));
            else if (dest.equals(new Position("c1")))
                temp = chessBoard.getPiece(new Position("a1"));
            else if (dest.equals(new Position("c8")))
                temp = chessBoard.getPiece(new Position("a8"));
            else if (dest.equals(new Position("g8")))
                temp = chessBoard.getPiece(new Position("h8"));
            else {
                throw new Exception("[ERROR] Cannot make this castle!");
            }
            if (!(temp instanceof Rook)) {
                throw new Exception("[ERROR] Cannot do this move!");
            }

            rook = (Rook) temp;
            if (!king.canCastle(rook)) {
                throw new Exception("That castling is not allowed!");
            }

            // Make the castling move
            ep_capture = null;
            ep_pawn = null;
            int direction = rook.getPosition().getColumn() > king.getPosition().getColumn() ? 90 : -90;
            Position position = king.getPosition().getAdjacentPosition(direction).getAdjacentPosition(direction);
            chessBoard.putPiece(position, king);
            chessBoard.putPiece(position.getAdjacentPosition(-direction), rook);

            // Update chessboard
            for (Piece p : pieces) {
                p.getMoves();
            }
            updatePositions();

            // Add move to moveSequence
            Move.Special special = (dest.getColumn() == 2) ? Move.Special.QUEENSIDE_CASTLE
                    : Move.Special.KINGSIDE_CASTLE;

            moveSequence.add(new Move(piece, src, dest, special));

        } // end piece instance King
        else {
            if (!piece.canMove(dest))
                throw new Exception("[ERROR] Cannot make that move!");

            Move m = new Move(src, dest);
            if (isInCheck() && !escapeMoves.contains(m)) {
                throw new Exception("[ERROR] Cannot make that move! king in check!");
            }

            // Prepare the move
            Move move = new Move(piece, src, dest);

            // Move the piece
            Piece target = chessBoard.putPiece(dest, piece);
            if (piece instanceof Pawn) {
                if (dest.equals(ep_capture)) {
                    // En passant capture
                    target = ep_pawn;
                    chessBoard.remove(ep_pawn.getPosition());
                    move.setSpecial(Move.Special.EN_PASSANT);
                    ep_pawn = null;
                    ep_capture = null;
                } else if (Math.abs(src.getLine() - dest.getLine()) == 2) {
                    // En passant move
                    ep_capture = new Position(dest.getLine() + (piece.isWhite() ? -1 : 1), src.getColumn());
                    ep_pawn = (Pawn) piece;
                }
            }
            if (target != null) {
                pieces.remove(target);
            }
            updatePositions();

            // Cannot make a move that leaves our King inCheck
            if (isInCheck()) {
                chessBoard.putPiece(src, piece);
                if (target != null) {
                    chessBoard.putPiece(dest, target);
                    pieces.add(target);
                } else {
                    chessBoard.remove(dest);
                    updatePositions();
                }
                escapeMoves = getEscapeMoves();
                Log.i(">>>>>>>>>>>>> MOVE", move.toString());
                throw new Exception("[ERROR] Cannot make that move! Leaves king in check!");
            }

            // Add move to moveSequence
            if (moveSequence.add(move)) {
                Log.i(">>>>>>>>>>>>> MOVE", move.toString());
            }
        }

        // Switch turn
        swapActivePlayer();

        boolean inCheck = isInCheck();
        for (Piece p : pieces) {
            p.update();
        }
        if (inCheck) {
            escapeMoves = getEscapeMoves();
        } else {
            escapeMoves = null;
            updateAllowedMoves();
        }

        return inCheck;
    }

    private boolean move(Position src, Position dest, char promotion) throws Exception {

        if (!chessBoard.isValidPosition(src) || !chessBoard.isValidPosition(dest)) {
            return false;
        }

        Piece p = chessBoard.getPiece(src);
        if (!(p instanceof Pawn)) {
            throw new Exception("\"" + promotion + "\" is not a proper input " + " for this move.");
        }
        if (dest.getLine() != 7 && dest.getLine() != 0) {
            throw new Exception("That's not a valid promotion." + dest.getLine());
        }
        Pawn pawn = (Pawn) p;
        if (!pawn.canMove(dest)) {
            throw new Exception("Pawn cant move for this position.");
        }

        Piece upgrade = new Queen(pawn.isWhite(), this, dest);

        // Make the move
        Piece target = chessBoard.putPiece(dest, upgrade);
        pieces.add(upgrade);
        Piece pawnToRemove = chessBoard.getPiece(src);
        pieces.remove(pawnToRemove);

        chessBoard.remove(src);
        if (target != null) {
            pieces.remove(target);
        }

        // Switch turn
        swapActivePlayer();

        // Add move to moveSequence
        moveSequence.add(new Move(p, src, dest, Move.Special.parseChar(promotion)));

        boolean inCheck = isInCheck();
        for (Piece piece : pieces) {
            piece.update();
        }
        if (inCheck) {
            escapeMoves = getEscapeMoves();
        } else {
            escapeMoves = null;
            updateAllowedMoves();
        }

        return inCheck;
    }

    public void setWinner(int winnerPlayer, int looserPlayer) {
        this.gameRunning = false;
        this.winnerPlayer = winnerPlayer;
        this.looserPlayer = looserPlayer;
    }

    public void setTimerOn(boolean timerOn) {
        this.timerOn = timerOn;
    }

    public void setTimer(int player, int time) {
        this.time[player] = time;
    }

    public ChessBoard getChessBoard() {
        return chessBoard;
    }

    public boolean isTimerOn() {
        return timerOn;
    }

    public int getTimer(int player) {
        return time[player];
    }

    public void incTimer(int player) {
        time[player]--;
    }

    public void setInitialTimer(int initialTimer) {
        this.time[0] = this.time[1] = initialTimer;
    }

    public void setGameRunning(boolean gameRunning) {
        this.gameRunning = gameRunning;
    }

    public boolean getGameRunning() {
        return gameRunning;
    }

    public ArrayList<Move> getMoveSequence() {
        return moveSequence;
    }

    public void setGameMode(int gameMode) {
        this.gameMode = gameMode;
    }

    public boolean dummyAiMOve() {
        ArrayList<Move> moves = new ArrayList<>();

        for (Map.Entry<Position, Piece> e : chessBoard.getEntrySet()) {
            Piece p = e.getValue();

            if (isPlayerOne() != p.isWhite())
                continue;

            for (Position pos : p.getMoves()) {
                if (pos != null)
                    moves.add(new Move(p.getPosition(), pos));
            }
        }
        if (moves.isEmpty())
            throw new IllegalArgumentException("No moves.");

        boolean checkMove = false;
        do {

            try {
                Move m = moves.get((int) (Math.random() * moves.size()));

                move(m.getSrc(), m.getDest());
                checkMove = true;
            } catch (Exception e) {
                e.printStackTrace();
                checkMove = false;
            }
        } while (!checkMove);
        return true;
    }

}
