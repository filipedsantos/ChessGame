package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;

import pt.isec.a21180878.chessgame.logic.board.ChessBoard;
import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 17-Nov-17.
 */

public abstract class Piece implements Serializable {

    private boolean isWhite;
    private Game game;
    private Position position;
    private ArrayList<Position> moves;

    public Piece(boolean isWhite, Game game, Position position) {
        super();
        if(game == null)
            throw new IllegalArgumentException("[ERROR] IllegalArgumentException -- Game is null!\n");

        this.isWhite = isWhite;
        this.game = game;
        this.position = position;
        if(position != null)
            this.moves = getMovePositions();
    }

    public String getAlgebraicName(){
        return "" + Character.toUpperCase(getPieceShortName());
    }

    public String getPieceName(){
        return "" + (isWhite() ? 'w': 'b') + Character.toUpperCase(getPieceShortName());
    }

    public abstract char getPieceShortName();

    public boolean isWhite() {
        return isWhite;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position pos) {
        this.position = pos;
    }

    public abstract ArrayList<Position> getAttackedPositions();

    public abstract ArrayList<Position> getMovePositions();

    public ChessBoard getChessBoard() {
        return game.getChessBoard();
    }

    public Game getGame() {
        return game;
    }

    public boolean canMove(Position p) {
        moves = getMovePositions();
        return moves.contains(p);
    }

    public boolean isSameColor(Piece piece) {
        return this.isWhite == piece.isWhite;
    }

    public void update() { this.moves = getMovePositions(); }

    public ArrayList<Position> getMoves() {
        return moves;
    }

    public abstract int getValue();


    @Override
    public String toString() {
        return "" + this.isWhite() + " : " + this.position;
    }
}
