package pt.isec.a21180878.chessgame.utils;

import java.io.Serializable;

/**
 * Created by Filipe on 22-Dec-17.
 */

public class Profile implements Serializable {

    private static final String defaultName = "Profile";

    private String profileName;
    private String mProfilePhotoPath;
    private int nGames;
    private int nMultiplayerGames;
    private int nWins;
    private int nDefeats;

    private boolean gameTimer = false;
    private  int gameTimerInMinutes = 0;

    public Profile() {
        this.profileName = defaultName;
        this.nGames = 0;
        this.nMultiplayerGames = 0;
        this.nWins = 0;
        this.nDefeats = 0;
    }

    public Profile(String profileName) {
        this.profileName = profileName;
        this.nGames = 0;
        this.nMultiplayerGames = 0;
        this.nWins = 0;
        this.nDefeats = 0;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public int getnGames() {
        return nGames;
    }

    public void setnGames(int nGames) {
        this.nGames = nGames;
    }

    public int getnMultiplayerGames() {
        return nMultiplayerGames;
    }

    public void setnMultiplayerGames(int nMultiplayerGames) {
        this.nMultiplayerGames = nMultiplayerGames;
    }

    public int getnWins() {
        return nWins;
    }

    public void setnWins(int nWins) {
        this.nWins = nWins;
    }

    public int getnDefeats() {
        return nDefeats;
    }

    public void setnDefeats(int nDefeats) {
        this.nDefeats = nDefeats;
    }

    public String getProfilePhotoPath() {
        return mProfilePhotoPath;
    }

    public void setProfilePhotoPath(String mProfilePhotoPath) {
        this.mProfilePhotoPath = mProfilePhotoPath;
    }

    public void setGameTimer(boolean gameTimer) {
        this.gameTimer = gameTimer;
    }

    public boolean getGameTimer() {
        return gameTimer;
    }

    public void setGameTimerInMinutes(int gameTimerInMinutes) {
        this.gameTimerInMinutes = gameTimerInMinutes;
    }

    public int getGameTimerInMinutes() {
        return gameTimerInMinutes;
    }
}
