package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 22-Nov-17.
 */

public class Knight extends Piece implements Serializable {

    public Knight(boolean isWhite, Game game, Position pos) {
        super(isWhite, game, pos);
    }

    @Override
    public char getPieceShortName() {
        return isWhite() ? 'N' : 'n';
    }

    @Override
    public ArrayList<Position> getAttackedPositions() {
        ArrayList<Position> positions = new ArrayList<Position>();
        for(int dir = 0; dir < 360; dir += 90) {
            Position p = getPosition().getAdjacentPosition(dir).getAdjacentPosition(dir);
            positions.add(p.getAdjacentPosition(dir + 90));
            positions.add(p.getAdjacentPosition(dir - 90));
        }

        ArrayList<Position> bad = new ArrayList<>(positions.size());
        for(Position p : positions) {
            if(!getChessBoard().isValidPosition(p))
                bad.add(p);
        }
        positions.removeAll(bad);
        return positions;
    }

    @Override
    public ArrayList<Position> getMovePositions() {
        ArrayList<Position> positions = new ArrayList<Position>();
        for(int dir = 0; dir < 360; dir += 90) {
            Position p = getPosition().getAdjacentPosition(dir).getAdjacentPosition(dir);
            Position    p1 = p.getAdjacentPosition(dir + 90),
                        p2 = p.getAdjacentPosition(dir - 90);

            try {
                if(getChessBoard().isValidPosition(p1) && (!getChessBoard().isOccupied(p1) || !isSameColor(getChessBoard().getPiece(p1))))
                    positions.add(p1);
                if(getChessBoard().isValidPosition(p2) && (!getChessBoard().isOccupied(p2) || !isSameColor(getChessBoard().getPiece(p2))))
                    positions.add(p2);
            }
            catch(NullPointerException e) {
                System.out.println("Null pointer exception found for " + this);
                System.out.println("At position " + p1 + ": " + getChessBoard().getPiece(p1));
                System.out.println("At position " + p2 + ": " + getChessBoard().getPiece(p2));
            }
        }
        return positions;
    }

    @Override
    public int getValue() {
        return 30;
    }
}
