package pt.isec.a21180878.chessgame.logic;

import java.io.Serializable;
import java.util.Objects;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.pieces.Piece;

public class Move implements Serializable {

    private static final long serialVersionUID = 40L;

    public enum Special {
        /**
         * Indicates that the move is a pawn underpromotion to a bishop.
         */
        BISHOP("B"),

        /**
         * Indicates that the move put the opponent in check.
         */
        CHECK("+"),

        /**
         * Indicates that the move put the opponent in checkmate.
         */
        CHECKMATE("#"),

        /**
         * Indicates that the move is an en passant capture.
         */
        EN_PASSANT("e.p."),

        /**
         * Indicates that the move is a kingside castle.
         */
        KINGSIDE_CASTLE("O-O"),

        /**
         * Indicates that the move is a pawn underpromotion to a knight.
         */
        KNIGHT("N"),

        /**
         * Indicates that this move is a normal move.
         */
        NONE(""),

        /**
         * Indicates that the move is a pawn promotion to a queen.
         */
        QUEEN("Q"),

        /**
         * Indicates that the move is a queensde castle.
         */
        QUEENSIDE_CASTLE("O-O-O"),

        /**
         * Indicates that the move is a pawn underpromotion to a rook.
         */
        ROOK("R");

        private String desc;
        private Special(String desc) {
            this.desc = desc;
        }

        /**
         * @return A description of this special move.
         */
        public String getDescription() {
            return desc;
        }

        /**
         * @param c A character.
         * @return Converts the character to a corresponding piece promotion.
         */
        public static Special parseChar(char c) {
            switch(Character.toLowerCase(c)) {
                case 'q':
                    return QUEEN;
                case 'r':
                    return ROOK;
                case 'n':
                    return KNIGHT;
                case 'b':
                    return BISHOP;
                default:
                    throw new IllegalArgumentException("Unexpected argument.");
            }
        }
    }

    private Special special;
    private Position src, dest;
    private Piece pieceSrc, pieceDest;


    public Move(Position src, Position dest) {
        this(src, dest, Special.NONE);
    }

    public Move(Position src, Position dest, Special special) {
        super();
        this.src =  src;
        this.dest = dest;
        this.special = special;
    }

    public Move(Piece piece, Position src, Position dest, Special special) {
        super();
        this.pieceSrc = piece;
        this.src =  src;
        this.dest = dest;
        this.special = special;

    }

    public Move(Piece piece, Position src, Position dest) {
        super();
        this.pieceSrc = piece;
        this.src =  src;
        this.dest = dest;
        this.special = Special.NONE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return move.src.equals(src) && move.dest.equals(dest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(special, src, dest);
    }

    public void setSpecial(Special special) {
        this.special = special;
    }

    public Special getSpecial() {
        return special;
    }

    public Position getSrc() {
        return src;
    }

    public Position getDest() {
        return dest;
    }

    @Override
    public String toString() {
        return pieceSrc.getPieceName() + "-" + src.getChessPosition() + dest.getChessPosition()
                + ((getSpecial() == Special.NONE) ? "" : (" ("+ getSpecial()) + ")") + " * ";
    }
}
