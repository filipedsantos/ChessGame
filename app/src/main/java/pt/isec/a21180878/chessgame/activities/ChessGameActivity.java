package pt.isec.a21180878.chessgame.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Date;
import java.util.Enumeration;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.logic.board.ChessBoard;
import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;
import pt.isec.a21180878.chessgame.logic.pieces.Piece;
import pt.isec.a21180878.chessgame.utils.Constants;
import pt.isec.a21180878.chessgame.utils.GameLog;
import pt.isec.a21180878.chessgame.utils.MyApplication;
import pt.isec.a21180878.chessgame.utils.Profile;
import pt.isec.a21180878.chessgame.utils.ServerDialog;

public class ChessGameActivity extends Activity implements Serializable, Constants, View.OnClickListener {
    private final static String TAG = "ChessGameActivity";
    private final static int BOARDSIZE = 8;
    private static final int PORT = 8899;
    public Handler messageHandler = new MessageHandler();
    private ProgressDialog pd = null;
    private ServerDialog serverDialog;
    private ImageButton[][] imageButtons;
    private ImageButton ibSelected;
    private Position selectedPosition;
    private TextView tvPlayerOne, tvPlayerTwo;
    private ImageView ivPlayerOne, ivPlayerTwo;
    private LinearLayout llPlayer1, llPlayer2;
    private Context context;
    private Profile profile;
    private Game game;
    private int gameMode;
    private int typeOnlineGame;
    private GameService mGameService;
    private boolean mBound = false;

    public TextView tvPlayerOneClock, tvPlayerTwoClock;
    boolean timerRunning = false;

    public Profile getProfile() {
        return profile;
    }

    Thread task = new Thread(new Runnable() {
        @Override
        public void run() {
            while (timerRunning && (game.getTimer(0) > 0 && game.getTimer(1) > 0)) {
                try {
                    Thread.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (game.getActivePlayer() == PLAYER_ONE) {
                                tvPlayerOneClock.setText(transformTime(game.getTimer(0)));
                                game.incTimer(0);
                            } else {
                                tvPlayerTwoClock.setText(transformTime(game.getTimer(1)));
                                game.incTimer(1);
                            }
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    public String transformTime(int i) {
        return Integer.toString(i / 60) + ":"
                + (Integer.toString(i % 60).length() == 1 ? ("0" + Integer.toString(i % 60)) : Integer.toString(i % 60));

    }

    // Service Connection --------------------------------------------------------------------------
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to GameService, cast the IBinder and get GameService instance
            GameService.GameServiceBinder binder = (GameService.GameServiceBinder) service;
            mGameService = binder.getService();
            mBound = true;
            Log.e(TAG, "onServiceConnected");

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.e(TAG, "onServiceDisconnected");
            mGameService = null;
            mBound = false;
        }
    };

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    // Activity LifeCycle Functions ----------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess_game);
        this.context = this;

        tvPlayerOne = findViewById(R.id.tvPlayer1);
        tvPlayerTwo = findViewById(R.id.tvPlayer2);
        ivPlayerOne = findViewById(R.id.iv_player1);
        ivPlayerTwo = findViewById(R.id.iv_player2);
        tvPlayerOneClock = findViewById(R.id.tvPlayerOneClock);
        tvPlayerTwoClock = findViewById(R.id.tvPlayerTwoClock);
        llPlayer1 = findViewById(R.id.llPlayer1);
        llPlayer2 = findViewById(R.id.llPlayer2);
        llPlayer1.setOnClickListener(this);
        llPlayer2.setOnClickListener(this);

        // Create ImageButtons refs and create listeners to the buttons
        imageButtons = initializeButtons();
        deleteImages();

        // If we have a saved state then we can restore it now
        if (savedInstanceState != null) {
            this.game = (Game) savedInstanceState.getSerializable(SAVEDGAME);
            this.gameMode = savedInstanceState.getInt(GAMEMODE);
            this.typeOnlineGame = savedInstanceState.getInt(TYPEONLINEGAME);
            this.game.setTimer(0, savedInstanceState.getInt(TIMEPLAYER1));
            this.game.setTimer(1, savedInstanceState.getInt(TIMEPLAYER2));
            this.timerRunning = savedInstanceState.getBoolean(TIMERON);

            if (game.isTimerOn()) {
                tvPlayerOneClock.setText(transformTime(game.getTimer(0)));
                tvPlayerTwoClock.setText(transformTime(game.getTimer(0)));
                tvPlayerOneClock.setVisibility(View.VISIBLE);
                tvPlayerTwoClock.setVisibility(View.VISIBLE);
                task.start();
            }

        } else {
            Intent intent = getIntent();
            if (intent != null)
                gameMode = intent.getIntExtra(GAMEMODE, -1);

            MyApplication myApplication = (MyApplication) getApplicationContext();
            profile = myApplication.getProfile();

            game = new Game(gameMode);

            switch (gameMode) {
                case SINGLEPLAYER:
                    game.setPlayer(PLAYER_ONE, profile);
                    game.setPlayer(PLAYER_TWO, new Profile("DummyBot"));
                    break;
                case MULTIPLAYER_OFFLINE:
                    createAndDisplayDialog();
                    game.setPlayer(PLAYER_ONE, profile);
                    break;
                case MULTIPLAYER_ONLINE:
                    // Show Multiplayer Dialogs
                    serverClientDialog();

                    break;
                default:
                    break;
            }
        }

        try {
            refreshBoard();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
        tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());

        MyApplication myApplication = (MyApplication) getApplicationContext();
        Profile profile = myApplication.getProfile();
        if (game.getPlayer(PLAYER_ONE).getProfile().getProfilePhotoPath() != null) {
            setPic(ivPlayerOne, profile.getProfilePhotoPath(), 200, 200);
            ivPlayerOne.setImageDrawable(null);
        }
    }

    public void setPic(ImageView view, String mCurrentPhotoPath, int reqWidth, int reqHeight) {
        // Get the dimensions of the View
        int targetW = 0;
        int targetH = 0;

        if (view != null) {
            targetW = view.getWidth();
            targetH = view.getHeight();
        }
        if (targetH == 0 || targetW == 0) {
            targetW = reqWidth;
            targetH = reqHeight;
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        BitmapDrawable bd = new BitmapDrawable(view.getResources(), bitmap);
        view.setBackground(bd);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (gameMode == MULTIPLAYER_ONLINE) {
            // Check if is it network available
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                Toast.makeText(this, "No network!!", Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            // Bind to LocalService
            Intent serviceIntent = new Intent(this, GameService.class);
            serviceIntent.putExtra("MESSENGER", new Messenger(messageHandler));
            startService(serviceIntent);
            bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timerRunning = false;
        if (task.isAlive()) {
            try {
                task.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (isFinishing()) {
            Intent serviceIntent = new Intent(this, GameService.class);
            stopService(serviceIntent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVEDGAME, game);
        outState.putInt(GAMEMODE, gameMode);
        outState.putInt(TYPEONLINEGAME, typeOnlineGame);
        outState.putInt(TIMEPLAYER1, game.getTimer(0));
        outState.putInt(TIMEPLAYER2, game.getTimer(1));
        outState.putBoolean(TIMERON, timerRunning);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit game ?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }

                        })
                .setNegativeButton("No", null).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llPlayer1:
                if (game.getGameMode() == SINGLEPLAYER) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_join, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_join:
                                    game.setGameMode(MULTIPLAYER_OFFLINE);
                                    game.setPlayer(PLAYER_ONE, new Profile(("Player")));
                                    tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                } else if (game.getGameMode() == MULTIPLAYER_OFFLINE) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_resign, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_resign:
                                    game.setGameMode(SINGLEPLAYER);
                                    game.setPlayer(PLAYER_ONE, new Profile(("DummyBot")));
                                    tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                } else if (game.getGameMode() == MULTIPLAYER_ONLINE && typeOnlineGame == SERVER) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_resign, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_resign:
                                    game.setGameMode(SINGLEPLAYER);
                                    game.setPlayer(PLAYER_ONE, new Profile(("DummyBot")));
                                    tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                } else {
                    llPlayer1.setClickable(false);
                }
                break;
            case R.id.llPlayer2:
                if (game.getGameMode() == SINGLEPLAYER) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_join, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_join:
                                    game.setGameMode(MULTIPLAYER_OFFLINE);
                                    game.setPlayer(PLAYER_TWO, new Profile(("Player")));
                                    tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                } else if (game.getGameMode() == MULTIPLAYER_OFFLINE) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_resign, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_resign:
                                    game.setGameMode(SINGLEPLAYER);
                                    game.setPlayer(PLAYER_TWO, new Profile(("DummyBot")));
                                    tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                } else if (game.getGameMode() == MULTIPLAYER_ONLINE && typeOnlineGame == CLIENT) {
                    PopupMenu popup = new PopupMenu(ChessGameActivity.this, v);
                    popup.getMenuInflater().inflate(R.menu.menu_resign, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_resign:
                                    game.setGameMode(SINGLEPLAYER);
                                    game.setPlayer(PLAYER_TWO, new Profile(("DummyBot")));
                                    tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                } else {
                    llPlayer2.setClickable(false);
                }
                break;
        }

    }

    // Dialogs -------------------------------------------------------------------------------------

    private void createAndDisplayDialog() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LinearLayout layout = new LinearLayout(this);

        TextView tvMessage = new TextView(this);
        final EditText etInput = new EditText(this);

        LinearLayout timerLayout = new LinearLayout(this);
        TextView tvTimer = new TextView(this);
        ToggleButton tbTimer = new ToggleButton(this);
        EditText etTimer = new EditText(this);

        tvMessage.setText("Enter a name for Player 2:");
        tvMessage.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
        etInput.setSingleLine();
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(tvMessage);
        layout.addView(etInput);
        etInput.setHint("Player 2");
        layout.setPadding(50, 40, 50, 10);

        tvTimer.setText("Timer");
        tvTimer.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, (float) 1.0));
        tvTimer.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvTimer.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);

        tbTimer.setChecked(false);
        tbTimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etTimer.setText("10:00");
                    etTimer.setVisibility(View.VISIBLE);
                } else {
                    etTimer.setVisibility(View.GONE);
                }
            }
        });

        etTimer.setText("10:00");
        etTimer.setSelection(etTimer.getText().length());
        etTimer.setVisibility(View.GONE);
        etTimer.setMaxLines(1);
        etTimer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(5);
        etTimer.setFilters(filterArray);
        etTimer.selectAll();
        etTimer.addTextChangedListener(new TextWatcher() {
            boolean erase = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    erase = true;
                }
                if (s.length() == 2 && erase) {
                    erase = false;
                    etTimer.setText(s + ":");
                    etTimer.setSelection(etTimer.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        timerLayout.setOrientation(LinearLayout.HORIZONTAL);
        timerLayout.addView(tvTimer);
        timerLayout.addView(tbTimer);
        timerLayout.addView(etTimer);
        layout.addView(timerLayout);

        builder.setView(layout);

        builder.setPositiveButton("Start Game", (dialog, which) -> {
            String name = etInput.getText().toString();
            if (name.length() < 2) {
                game.setPlayer(PLAYER_TWO, new Profile("Player 2"));
                tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
            } else {
                game.setPlayer(PLAYER_TWO, new Profile(name));
                tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());

            }
            if (tbTimer.isChecked()) {
                startTimer(etTimer.getText().toString());
            }
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        builder.create().show();
    }

    public void startTimer(String s) {
        int minutes = 0;

        String[] arr = s.split(":");
        if (arr.length == 2) {
            minutes = Integer.parseInt(arr[0]) * 60 + Integer.parseInt(arr[1]);
        } else {
            minutes = Integer.parseInt(arr[0]) * 60;
        }

        timerRunning = true;
        game.setTimerOn(true);
        game.setInitialTimer(minutes);
        tvPlayerOneClock.setText(s);
        tvPlayerTwoClock.setText(s);
        tvPlayerOneClock.setVisibility(View.VISIBLE);
        tvPlayerTwoClock.setVisibility(View.VISIBLE);
        if (!task.isAlive())
            task.start();

    }

    private void startTimer(int timer) {
        game.setInitialTimer(timer);
        tvPlayerOneClock.setVisibility(View.VISIBLE);
        tvPlayerTwoClock.setVisibility(View.VISIBLE);
        tvPlayerOneClock.setText(transformTime(game.getTimer(0)));
        tvPlayerTwoClock.setText(transformTime(game.getTimer(1)));
        if (!task.isAlive())
            task.start();
    }

    private void serverClientDialog() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle("Client/Server?")
                .setNegativeButton("Server", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        game.setPlayer(PLAYER_ONE, profile);
                        game.getPlayer(PLAYER_ONE).setOnlinePlayerType(SERVER);
                        typeOnlineGame = SERVER;
                        tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
                        serverDialog();
                    }
                })
                .setPositiveButton("Client", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        game.setPlayer(PLAYER_TWO, profile);
                        game.getPlayer(PLAYER_TWO).setOnlinePlayerType(CLIENT);
                        tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
                        typeOnlineGame = CLIENT;
                        clientDialog();

                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                }).create();
        ad.show();
    }

    private void serverDialog() {
        String ip = getLocalIpAddress();

       /* pd = new ProgressDialog(this);
        pd.setTitle("Chess Server");
        pd.setMessage("Waiting for a client..." + "\n(IP: " + ip + ")");
        pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        pd.show();
        mGameService.server();*/

        serverDialog = new ServerDialog(context, ip, this);
        serverDialog.show();
        mGameService.server();
    }

    private void clientDialog() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final EditText edtIP = new EditText(this);
        edtIP.setText(stringIP); // default ip
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Chess Client");
        builder.setMessage("Server IP");
        builder.setView(edtIP);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mGameService.client(edtIP.getText().toString(), PORT);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        AlertDialog ad = builder.create();
        ad.show();
    }

    private void showEndGameDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Victory !!!");
        builder.setMessage(game.getPlayer(game.getWinnerPlayer()).getProfile().getProfileName() + " wins !");
        builder.setPositiveButton("New Game", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                game = new Game(game);
                try {
                    refreshBoard();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        AlertDialog ad = builder.create();
        ad.show();
    }

    private void showCloseConnectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connection Lost");
        builder.setMessage("Do u want to keep playing ??");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                game.setGameMode(SINGLEPLAYER);
                if (typeOnlineGame == SERVER) {
                    game.setPlayer(PLAYER_TWO, new Profile(("DummyBot")));
                    tvPlayerTwo.setText(game.getPlayer(PLAYER_TWO).getProfile().getProfileName());
                } else {
                    game.setPlayer(PLAYER_ONE, new Profile(("DummyBot")));
                    tvPlayerOne.setText(game.getPlayer(PLAYER_ONE).getProfile().getProfileName());
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        AlertDialog ad = builder.create();
        ad.show();
    }


    // Functions -----------------------------------------------------------------------------------

    private void deleteImages() {
        for (int l = 0; l < BOARDSIZE; l++) {
            for (int c = 0; c < BOARDSIZE; c++) {
                imageButtons[l][c].setImageResource(R.drawable.blank);
            }
        }
    }

    /**
     * Refresh the entire board to the screen.
     */
    private void refreshBoard() throws Exception {

        ChessBoard chessBoard = game.getChessBoard();
        for (int l = 0; l < BOARDSIZE; l++) {
            for (int c = 0; c < BOARDSIZE; c++) {

                ImageButton ib = imageButtons[l][c];

                Position position = new Position(l, c);

                if (chessBoard.isOccupied(position)) {
                    Piece piece = chessBoard.getPiece(position);
                    if (piece.getAlgebraicName().equals("K")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wk : R.drawable.bk);
                    } else if (piece.getAlgebraicName().equals("Q")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wq : R.drawable.bq);
                    }
                    if (piece.getAlgebraicName().equals("B")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wb : R.drawable.bb);
                    }
                    if (piece.getAlgebraicName().equals("N")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wn : R.drawable.bn);
                    }
                    if (piece.getAlgebraicName().equals("R")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wr : R.drawable.br);
                    }
                    if (piece.getAlgebraicName().equals("P")) {
                        ib.setImageResource(piece.isWhite() ? R.drawable.wp : R.drawable.bp);
                    }
                } else {
                    ib.setImageResource(R.drawable.blank);
                }
            }
        }
    }

    private ImageButton[][] initializeButtons() {
        ImageButton[][] imageButtons = new ImageButton[BOARDSIZE][BOARDSIZE];

        imageButtons[7][0] = findViewById(R.id.ib_a8);
        imageButtons[7][1] = findViewById(R.id.ib_b8);
        imageButtons[7][2] = findViewById(R.id.ib_c8);
        imageButtons[7][3] = findViewById(R.id.ib_d8);
        imageButtons[7][4] = findViewById(R.id.ib_e8);
        imageButtons[7][5] = findViewById(R.id.ib_f8);
        imageButtons[7][6] = findViewById(R.id.ib_g8);
        imageButtons[7][7] = findViewById(R.id.ib_h8);

        imageButtons[6][0] = findViewById(R.id.ib_a7);
        imageButtons[6][1] = findViewById(R.id.ib_b7);
        imageButtons[6][2] = findViewById(R.id.ib_c7);
        imageButtons[6][3] = findViewById(R.id.ib_d7);
        imageButtons[6][4] = findViewById(R.id.ib_e7);
        imageButtons[6][5] = findViewById(R.id.ib_f7);
        imageButtons[6][6] = findViewById(R.id.ib_g7);
        imageButtons[6][7] = findViewById(R.id.ib_h7);

        imageButtons[5][0] = findViewById(R.id.ib_a6);
        imageButtons[5][1] = findViewById(R.id.ib_b6);
        imageButtons[5][2] = findViewById(R.id.ib_c6);
        imageButtons[5][3] = findViewById(R.id.ib_d6);
        imageButtons[5][4] = findViewById(R.id.ib_e6);
        imageButtons[5][5] = findViewById(R.id.ib_f6);
        imageButtons[5][6] = findViewById(R.id.ib_g6);
        imageButtons[5][7] = findViewById(R.id.ib_h6);

        imageButtons[4][0] = findViewById(R.id.ib_a5);
        imageButtons[4][1] = findViewById(R.id.ib_b5);
        imageButtons[4][2] = findViewById(R.id.ib_c5);
        imageButtons[4][3] = findViewById(R.id.ib_d5);
        imageButtons[4][4] = findViewById(R.id.ib_e5);
        imageButtons[4][5] = findViewById(R.id.ib_f5);
        imageButtons[4][6] = findViewById(R.id.ib_g5);
        imageButtons[4][7] = findViewById(R.id.ib_h5);

        imageButtons[3][0] = findViewById(R.id.ib_a4);
        imageButtons[3][1] = findViewById(R.id.ib_b4);
        imageButtons[3][2] = findViewById(R.id.ib_c4);
        imageButtons[3][3] = findViewById(R.id.ib_d4);
        imageButtons[3][4] = findViewById(R.id.ib_e4);
        imageButtons[3][5] = findViewById(R.id.ib_f4);
        imageButtons[3][6] = findViewById(R.id.ib_g4);
        imageButtons[3][7] = findViewById(R.id.ib_h4);

        imageButtons[2][0] = findViewById(R.id.ib_a3);
        imageButtons[2][1] = findViewById(R.id.ib_b3);
        imageButtons[2][2] = findViewById(R.id.ib_c3);
        imageButtons[2][3] = findViewById(R.id.ib_d3);
        imageButtons[2][4] = findViewById(R.id.ib_e3);
        imageButtons[2][5] = findViewById(R.id.ib_f3);
        imageButtons[2][6] = findViewById(R.id.ib_g3);
        imageButtons[2][7] = findViewById(R.id.ib_h3);

        imageButtons[1][0] = findViewById(R.id.ib_a2);
        imageButtons[1][1] = findViewById(R.id.ib_b2);
        imageButtons[1][2] = findViewById(R.id.ib_c2);
        imageButtons[1][3] = findViewById(R.id.ib_d2);
        imageButtons[1][4] = findViewById(R.id.ib_e2);
        imageButtons[1][5] = findViewById(R.id.ib_f2);
        imageButtons[1][6] = findViewById(R.id.ib_g2);
        imageButtons[1][7] = findViewById(R.id.ib_h2);

        imageButtons[0][0] = findViewById(R.id.ib_a1);
        imageButtons[0][1] = findViewById(R.id.ib_b1);
        imageButtons[0][2] = findViewById(R.id.ib_c1);
        imageButtons[0][3] = findViewById(R.id.ib_d1);
        imageButtons[0][4] = findViewById(R.id.ib_e1);
        imageButtons[0][5] = findViewById(R.id.ib_f1);
        imageButtons[0][6] = findViewById(R.id.ib_g1);
        imageButtons[0][7] = findViewById(R.id.ib_h1);

        // Setup Listeners
        imageButtons[7][0].setOnClickListener(new onChessButton("a8", imageButtons[7][0]));
        imageButtons[7][1].setOnClickListener(new onChessButton("b8", imageButtons[7][1]));
        imageButtons[7][2].setOnClickListener(new onChessButton("c8", imageButtons[7][2]));
        imageButtons[7][3].setOnClickListener(new onChessButton("d8", imageButtons[7][3]));
        imageButtons[7][4].setOnClickListener(new onChessButton("e8", imageButtons[7][4]));
        imageButtons[7][5].setOnClickListener(new onChessButton("f8", imageButtons[7][5]));
        imageButtons[7][6].setOnClickListener(new onChessButton("g8", imageButtons[7][6]));
        imageButtons[7][7].setOnClickListener(new onChessButton("h8", imageButtons[7][7]));

        imageButtons[6][0].setOnClickListener(new onChessButton("a7", imageButtons[6][0]));
        imageButtons[6][1].setOnClickListener(new onChessButton("b7", imageButtons[6][1]));
        imageButtons[6][2].setOnClickListener(new onChessButton("c7", imageButtons[6][2]));
        imageButtons[6][3].setOnClickListener(new onChessButton("d7", imageButtons[6][3]));
        imageButtons[6][4].setOnClickListener(new onChessButton("e7", imageButtons[6][4]));
        imageButtons[6][5].setOnClickListener(new onChessButton("f7", imageButtons[6][5]));
        imageButtons[6][6].setOnClickListener(new onChessButton("g7", imageButtons[6][6]));
        imageButtons[6][7].setOnClickListener(new onChessButton("h7", imageButtons[6][7]));

        imageButtons[5][0].setOnClickListener(new onChessButton("a6", imageButtons[5][0]));
        imageButtons[5][1].setOnClickListener(new onChessButton("b6", imageButtons[5][1]));
        imageButtons[5][2].setOnClickListener(new onChessButton("c6", imageButtons[5][2]));
        imageButtons[5][3].setOnClickListener(new onChessButton("d6", imageButtons[5][3]));
        imageButtons[5][4].setOnClickListener(new onChessButton("e6", imageButtons[5][4]));
        imageButtons[5][5].setOnClickListener(new onChessButton("f6", imageButtons[5][5]));
        imageButtons[5][6].setOnClickListener(new onChessButton("g6", imageButtons[5][6]));
        imageButtons[5][7].setOnClickListener(new onChessButton("h6", imageButtons[5][7]));

        imageButtons[4][0].setOnClickListener(new onChessButton("a5", imageButtons[4][0]));
        imageButtons[4][1].setOnClickListener(new onChessButton("b5", imageButtons[4][1]));
        imageButtons[4][2].setOnClickListener(new onChessButton("c5", imageButtons[4][2]));
        imageButtons[4][3].setOnClickListener(new onChessButton("d5", imageButtons[4][3]));
        imageButtons[4][4].setOnClickListener(new onChessButton("e5", imageButtons[4][4]));
        imageButtons[4][5].setOnClickListener(new onChessButton("f5", imageButtons[4][5]));
        imageButtons[4][6].setOnClickListener(new onChessButton("g5", imageButtons[4][6]));
        imageButtons[4][7].setOnClickListener(new onChessButton("h5", imageButtons[4][7]));

        imageButtons[3][0].setOnClickListener(new onChessButton("a4", imageButtons[3][0]));
        imageButtons[3][1].setOnClickListener(new onChessButton("b4", imageButtons[3][1]));
        imageButtons[3][2].setOnClickListener(new onChessButton("c4", imageButtons[3][2]));
        imageButtons[3][3].setOnClickListener(new onChessButton("d4", imageButtons[3][3]));
        imageButtons[3][4].setOnClickListener(new onChessButton("e4", imageButtons[3][4]));
        imageButtons[3][5].setOnClickListener(new onChessButton("f4", imageButtons[3][5]));
        imageButtons[3][6].setOnClickListener(new onChessButton("g4", imageButtons[3][6]));
        imageButtons[3][7].setOnClickListener(new onChessButton("h4", imageButtons[3][7]));

        imageButtons[2][0].setOnClickListener(new onChessButton("a3", imageButtons[2][0]));
        imageButtons[2][1].setOnClickListener(new onChessButton("b3", imageButtons[2][1]));
        imageButtons[2][2].setOnClickListener(new onChessButton("c3", imageButtons[2][2]));
        imageButtons[2][3].setOnClickListener(new onChessButton("d3", imageButtons[2][3]));
        imageButtons[2][4].setOnClickListener(new onChessButton("e3", imageButtons[2][4]));
        imageButtons[2][5].setOnClickListener(new onChessButton("f3", imageButtons[2][5]));
        imageButtons[2][6].setOnClickListener(new onChessButton("g3", imageButtons[2][6]));
        imageButtons[2][7].setOnClickListener(new onChessButton("h3", imageButtons[2][7]));

        imageButtons[1][0].setOnClickListener(new onChessButton("a2", imageButtons[1][0]));
        imageButtons[1][1].setOnClickListener(new onChessButton("b2", imageButtons[1][1]));
        imageButtons[1][2].setOnClickListener(new onChessButton("c2", imageButtons[1][2]));
        imageButtons[1][3].setOnClickListener(new onChessButton("d2", imageButtons[1][3]));
        imageButtons[1][4].setOnClickListener(new onChessButton("e2", imageButtons[1][4]));
        imageButtons[1][5].setOnClickListener(new onChessButton("f2", imageButtons[1][5]));
        imageButtons[1][6].setOnClickListener(new onChessButton("g2", imageButtons[1][6]));
        imageButtons[1][7].setOnClickListener(new onChessButton("h2", imageButtons[1][7]));

        imageButtons[0][0].setOnClickListener(new onChessButton("a1", imageButtons[0][0]));
        imageButtons[0][1].setOnClickListener(new onChessButton("b1", imageButtons[0][1]));
        imageButtons[0][2].setOnClickListener(new onChessButton("c1", imageButtons[0][2]));
        imageButtons[0][3].setOnClickListener(new onChessButton("d1", imageButtons[0][3]));
        imageButtons[0][4].setOnClickListener(new onChessButton("e1", imageButtons[0][4]));
        imageButtons[0][5].setOnClickListener(new onChessButton("f1", imageButtons[0][5]));
        imageButtons[0][6].setOnClickListener(new onChessButton("g1", imageButtons[0][6]));
        imageButtons[0][7].setOnClickListener(new onChessButton("h1", imageButtons[0][7]));


        return imageButtons;
    }

    private boolean getPositionColor(String pos) {
        int line = pos.charAt(0) - 'a';
        int column = pos.charAt(1) - '1';
        return ((line + column) % 2) != 0;
    }

    public Game getGame() {
        return game;
    }

    private void setGame(Game g) {
        this.game = g;
    }

    private void endGame(int winnerPlayer, int looserPlayer) {
        Toast.makeText(context, "CHECK MATE! Winner: " + game.getPlayer(winnerPlayer).getProfile().getProfileName(), Toast.LENGTH_SHORT).show();

        MyApplication myApplication = (MyApplication) getApplication();

        game.setGameRunning(false);
        game.setWinner(winnerPlayer, looserPlayer);

        showEndGameDialog();
        GameLog gameLog = new GameLog(
                game.getGameMode(),
                game.getPlayer(PLAYER_ONE).getProfile().getProfileName(),
                game.getPlayer(PLAYER_TWO).getProfile().getProfileName(),
                new Date(),
                game.getMoveSequence(),
                game.getWinnerPlayer() == PLAYER_ONE
        );
        myApplication.addGameLog(gameLog);

        profile.setnGames(profile.getnGames() + 1);
        if(game.getGameMode() != SINGLEPLAYER)
            profile.setnMultiplayerGames(profile.getnMultiplayerGames() +1);
        if (game.getWinnerPlayer() == PLAYER_ONE)
            profile.setnWins(profile.getnWins() + 1);
        else
            profile.setnDefeats(profile.getnDefeats() + 1);

        myApplication.saveProfile(profile);

    }

    private class onChessButton implements View.OnClickListener {
        private String position;
        private ImageButton imageButton;

        public onChessButton(String pos, ImageButton imageButton) {
            this.position = pos;
            this.imageButton = imageButton;
        }

        public String getPosition() {
            return "Pos: " + position;
        }

        @Override
        public void onClick(View view) {
            //Toast.makeText(context, "" + position.toString(), Toast.LENGTH_LONG).show();

            if (game.getGameRunning()) {
                switch (game.getGameMode()) {
                    case SINGLEPLAYER:
                        makeDummyAiMove();
                        break;
                    case MULTIPLAYER_OFFLINE:
                        moveMultiplayerOffline();
                        break;
                    case MULTIPLAYER_ONLINE:
                        moveMultiplayerOnline();
                        break;
                    default:
                        break;
                }
            }
        }

        private void makeDummyAiMove() {
            if (game.getActivePlayer() == PLAYER_ONE) {
                boolean myMove = move();
                if (myMove) {
                    try {
                        boolean dummyAiMOve = game.dummyAiMOve();
                        if (dummyAiMOve)
                            refreshBoard();

                        if (game.isCheckmate()) {
                            Toast.makeText(context, "CHECK MATE! Winner: " + game.getPlayer(game.getWinnerPlayer()).getProfile().getProfileName(), Toast.LENGTH_SHORT).show();
                            endGame(game.getWinnerPlayer(), game.getActivePlayer());
                        } else if (game.isInCheck()) {
                            Toast.makeText(context, "CHECK!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void moveMultiplayerOffline() {
            move();
        }

        private void moveMultiplayerOnline() {
            Log.i(">>>>>>>>>>>>>>>>", game.getActivePlayer() + " || " + typeOnlineGame);
            if (game.getActivePlayer() == typeOnlineGame) {
                if (move()) {
                    game.version = 11;
                    mGameService.send(game);
                }
            } else {
                Toast.makeText(context, "Not your time to play!", Toast.LENGTH_SHORT).show();
            }
        }

        private boolean move() {
            if (ibSelected != null) {
                if (imageButton == ibSelected) {

                    ibSelected.setBackgroundColor(getPositionColor(position) ? getResources().getColor(R.color.colorLightBlue) :
                            getResources().getColor(R.color.colorDarkBlue));

                    ibSelected = null;
                    selectedPosition = null;
                    return false;
                } else if (game.getChessBoard().getPiece(position) != null &&
                        game.getChessBoard().getPiece(position).isWhite() == game.getChessBoard().getPiece(selectedPosition).isWhite()) {

                    ibSelected.setBackgroundColor(getPositionColor(selectedPosition.toString()) ? getResources().getColor(R.color.colorLightBlue) :
                            getResources().getColor(R.color.colorDarkBlue));

                    ibSelected = imageButton;
                    selectedPosition = new Position(this.position);
                    ibSelected.setBackgroundColor(Color.GRAY);
                } else {
                    Position src = selectedPosition;
                    Position dest = new Position(position);

                    try {
                        game.move(src, dest);
                        refreshBoard();

                        if (game.isCheckmate()) {
                            Toast.makeText(context, "CHECK MATE! Winner: " + game.getPlayer(game.getWinnerPlayer()).getProfile().getProfileName(), Toast.LENGTH_SHORT).show();
                            endGame(game.getWinnerPlayer(), game.getActivePlayer());
                        } else if (game.isInCheck()) {
                            Toast.makeText(context, "CHECK!", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                        Log.e(">>>>>", e.toString());
                    } finally {
                        ibSelected.setBackgroundColor(getPositionColor(selectedPosition.toString())
                                ? getResources().getColor(R.color.colorLightBlue) :
                                getResources().getColor(R.color.colorDarkBlue));
                        ibSelected = null;
                        selectedPosition = null;
                    }
                }
            } else {
                Position pos = new Position(position);
                if (game.getChessBoard().getPiece(pos) == null) {
                    return false;
                }
                Piece p = game.getChessBoard().getPiece(pos);
                if (p.isWhite() != game.isPlayerOne()) {
                    Toast.makeText(
                            context,
                            "It's " + (game.getPlayer(game.getActivePlayer()).getProfile().getProfileName()) + " turn.",
                            Toast.LENGTH_LONG).show();
                    return false;
                }

                ibSelected = imageButton;
                selectedPosition = new Position(this.position);
                ibSelected.setBackgroundColor(Color.GRAY);
            }
            return false;
        }
    }

    public class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            int state = message.arg1;
            switch (state) {
                case MESSAGE_CLOSE_DIALOG:
                    serverDialog.dismiss();
                    break;
                case MESSAGE_REFRESH_BOARD:
                    Toast.makeText(context, "MESSAGE_REFRESH_BOARD", Toast.LENGTH_SHORT).show();
                    setGame((Game) message.obj);

                    try {
                        refreshBoard();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!game.getGameRunning())
                        endGame(game.getWinnerPlayer(), game.getActivePlayer());
                    break;
                case MESSAGE_CLOSE_CONNECTION:
                    Toast.makeText(context, "MESSAGE_CLOSE_CONNECTION", Toast.LENGTH_SHORT).show();
                    if (!isFinishing() && game.getGameRunning()) {
                        showCloseConnectionDialog();
                    } else {
                        finish();
                    }
                    break;
                case MESSAGE_CREATE_PROFILE:
                    Toast.makeText(context, "MESSAGE_CREATE_PROFILE ...", Toast.LENGTH_SHORT).show();
                    game.setPlayer(PLAYER_TWO, (Profile) message.obj);
                    tvPlayerTwo.setText(((Profile) message.obj).getProfileName());
                    game.version = 5;
                    mGameService.sendProfile(profile);
                    break;
                case MESSAGE_START_GAME:
                    Toast.makeText(context, "MESSAGE_START_GAME ...", Toast.LENGTH_SHORT).show();
                    //setGame((Game) message.obj);

                    Profile p = (Profile) message.obj;

                    game.setPlayer(PLAYER_ONE, p);
                    tvPlayerOne.setText(p.getProfileName());


                    if (p.getGameTimer()) {
                        game.setTimer(1,p.getGameTimerInMinutes());
                        startTimer(game.getTimer(1));
                        timerRunning = true;
                    }
                    break;
            }
        }
    }

}
