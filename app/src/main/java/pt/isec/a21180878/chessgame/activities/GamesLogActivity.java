package pt.isec.a21180878.chessgame.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.utils.Constants;
import pt.isec.a21180878.chessgame.utils.GameLog;
import pt.isec.a21180878.chessgame.utils.MyApplication;

public class GamesLogActivity extends Activity implements Constants {

    private Context context;
    private TextView tvNoGamesPlayed;
    private ListView lvGamesLog;
    private GamesLogAdapter gamesLogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games_log);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        initListViewData();
    }

    private void initListViewData() {
        this.context = this;
        tvNoGamesPlayed = findViewById(R.id.tv_gamesLog_empty);
        lvGamesLog = findViewById(R.id.lv_gamesLog);
        MyApplication myApplication = (MyApplication) getApplicationContext();
        ArrayList<GameLog> gameLogs = myApplication.getGamesLog();
        gamesLogAdapter = new GamesLogAdapter(gameLogs);
        lvGamesLog.setAdapter(gamesLogAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_delete:
                gamesLogAdapter.clearGameLog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class GamesLogAdapter extends BaseAdapter {

        private ArrayList<GameLog> gamesLog;

        public GamesLogAdapter(ArrayList<GameLog> gameLogs) {
            this.gamesLog = gameLogs;
            /*gameLogs.add(new GameLog(1, "Player1", "Player2", new Date(), false));
            gameLogs.add(new GameLog(1, "Player1", "Player2", new Date(), false));
            gameLogs.add(new GameLog(1, "Player1", "Player2", new Date(), false));
            MyApplication.saveGamesLog();*/
        }

        @Override
        public int getCount() {
            if (gamesLog.size() == 0) {
                tvNoGamesPlayed.setVisibility(View.VISIBLE);
            }
            return gamesLog.size();
        }

        @Override
        public Object getItem(int position) {
            return gamesLog.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;

            if (view == null) {
                // inflate the layout
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.lv_gameslog_adapter, viewGroup, false);
                // well set up the ViewHolder
                viewHolder = new ViewHolder(view);
                // store the holder with the view.
                view.setTag(viewHolder);
            } else {
                // we've just avoided calling findViewById() on resource everytime
                // just use the viewHolder
                viewHolder = (ViewHolder) view.getTag();
            }

            lvGamesLog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, GameLogDetailActivity.class);
                    intent.putExtra("GAME_LOG_DETAIL", position);
                    startActivity(intent);

                }
            });
            lvGamesLog.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    PopupMenu popup = new PopupMenu(GamesLogActivity.this, view);
                    popup.getMenuInflater().inflate(R.menu.menu_delete, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_delete:
                                    MyApplication.deleteGameLog(position);
                                    notifyDataSetChanged();
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                    notifyDataSetChanged();
                    return true;
                }
            });

            GameLog g = gamesLog.get(position);
            if (g != null) {

                switch (g.getGameMode()){
                    case SINGLEPLAYER:
                        viewHolder.ivGameMode.setBackground( getResources().getDrawable(R.drawable.ic_memory));
                        break;
                    case MULTIPLAYER_OFFLINE:
                        viewHolder.ivGameMode.setBackground( getResources().getDrawable(R.drawable.ic_group));
                        break;
                    case MULTIPLAYER_ONLINE:
                        viewHolder.ivGameMode.setBackground( getResources().getDrawable(R.drawable.ic_action_wifi));
                        break;
                }
                viewHolder.tvPlayer1.setText(g.getPlayer1() + " vs " + g.getPlayer2());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy");
                String time = simpleDateFormat.format(g.getDate());
                viewHolder.tvDate.setText(time);

                viewHolder.tvWin1.setTextSize(32);
                viewHolder.tvWin1.setTypeface(viewHolder.tvWin1.getTypeface(), Typeface.BOLD | Typeface.ITALIC);


                if(g.getWin()){
                    viewHolder.tvWin1.setText("W");
                    viewHolder.tvWin1.setTextColor(Color.GREEN);
                }
                else {
                    viewHolder.tvWin1.setText("L");
                    viewHolder.tvWin1.setTextColor(Color.RED);
                }

            }

            return view;
        }

        public void clearGameLog() {
            MyApplication.deleteGameLog();
            notifyDataSetChanged();
        }

        private class ViewHolder {
            LinearLayout vh_ll;
            TextView tvPlayer1, tvDate;
            TextView tvWin1;
            ImageView ivGameMode;

            public ViewHolder(View view) {
                this.vh_ll = view.findViewById(R.id.llLV);
                this.tvPlayer1 = (TextView) view.findViewById(R.id.tvPlayer);
                this.tvDate = (TextView) view.findViewById(R.id.tv_date);
                this.ivGameMode = (ImageView) view.findViewById(R.id.iv_gameMode);
                this.tvWin1 = (TextView) view.findViewById(R.id.tv_win1);
            }
        }
    }
}
