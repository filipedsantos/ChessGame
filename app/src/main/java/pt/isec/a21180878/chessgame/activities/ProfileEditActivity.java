package pt.isec.a21180878.chessgame.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.utils.MyApplication;
import pt.isec.a21180878.chessgame.utils.Profile;

public class ProfileEditActivity extends Activity {

    private static final int PICK_IMAGE_REQUEST = 1242;
    private EditText et_profileName;
    private ImageButton bt_takePicture;
    private ImageView ivPhoto;
    private Context context;
    private String profilePhotoPath;
    private Profile profile;

    public void profilePhotoPath(String profilePhoto) {
        profilePhotoPath = profilePhoto;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.context = this;
        et_profileName = findViewById(R.id.et_profileName);
        ivPhoto = findViewById(R.id.iv_profile_photo);
        bt_takePicture = findViewById(R.id.btn_takepicture);
        bt_takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent _intent = new Intent(context, CameraActivity.class);
                _intent.setType("image/*");
                _intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(_intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        MyApplication myApplication = (MyApplication) getApplicationContext();
        profile = myApplication.getProfile();
        et_profileName.setText(profile.getProfileName());

        if(profile.getProfilePhotoPath() != null) {
            profilePhotoPath = profile.getProfilePhotoPath();
            setPic(ivPhoto, profile.getProfilePhotoPath());
            ivPhoto.setImageDrawable(null);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_save:
                String profileName = et_profileName.getText().toString();
                if(profileName.length() > 2) {
                    profile.setProfileName(profileName);
                    try {
                        copyFile(getExternalFilesDir(null) + "/temp.jpg",getExternalFilesDir(null) + "/pic.jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    profilePhotoPath = (getExternalFilesDir(null) + "/pic.jpg");
                    profile.setProfilePhotoPath(profilePhotoPath);
                    MyApplication myApplication = (MyApplication) getApplicationContext();
                    myApplication.saveProfile(profile);
                    finish();
                }
                else {
                    et_profileName.requestFocus();
                    Toast.makeText(this, "Invalid profile name...", Toast.LENGTH_SHORT).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void copyFile(String sourceFile, String destFile) throws IOException {

        File sourceLocation = new File(sourceFile);
        File targetLocation = new File(destFile);

        if (!targetLocation.getParentFile().exists())
            targetLocation.getParentFile().mkdirs();

        if (!targetLocation.exists()) {
            targetLocation.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceLocation).getChannel();
            destination = new FileOutputStream(targetLocation).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST) {
            if(resultCode == RESULT_OK) {
                handleUserPickedImage(data);
            }
        }
    }

    private void handleUserPickedImage(Intent data) {
        if ((data != null) && (data.getData() != null)) {
            Uri _imageUri = data.getData();
            // Do something neat with the image...
            setPic(ivPhoto, _imageUri.getPath());
            ivPhoto.setImageDrawable(null);
        } else {
            // We didn't receive an image...
        }
    }

    public static void setPic(View view, String mCurrentPhotoPath) {
        // Get the dimensions of the View
        int targetW = view.getWidth();
        int targetH = view.getHeight();

        if (targetH == 0 || targetW == 0) {
            targetW = 400;
            targetH = 400;
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        BitmapDrawable bd = new BitmapDrawable(view.getResources(), bitmap);
        view.setBackground(bd);
    }

}
