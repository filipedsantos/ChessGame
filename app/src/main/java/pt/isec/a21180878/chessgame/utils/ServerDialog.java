package pt.isec.a21180878.chessgame.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.activities.ChessGameActivity;

/**
 * Created by Filipe on 08-Jan-18.
 */

public class ServerDialog extends Dialog {

    private EditText etTimer;
    private ToggleButton tbTimer;
    private ChessGameActivity activity;

    private String ip;

    public ServerDialog(@NonNull Context context, String ip, ChessGameActivity chessGameActivity) {
        super(context);

        this.ip = ip;
        this.activity = chessGameActivity;
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_server);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvIP = findViewById(R.id.tv_ip);
        etTimer = findViewById(R.id.et_timer);
        tbTimer = findViewById(R.id.tb_timer);

        tvIP.setText(ip);

        etTimer.setText("10:00");
        etTimer.setSelection(etTimer.getText().length());
        etTimer.setVisibility(View.GONE);
        etTimer.setMaxLines(1);
        etTimer.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(5);
        etTimer.setFilters(filterArray);
        etTimer.selectAll();
        etTimer.addTextChangedListener(new TextWatcher() {
            boolean erase = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    erase = true;
                }
                if (s.length() == 2 && erase) {
                    erase = false;
                    etTimer.setText(s + ":");
                    etTimer.setSelection(etTimer.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tbTimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etTimer.setText("10:00");
                    etTimer.setVisibility(View.VISIBLE);

                    String[] arr = etTimer.getText().toString().split(":");
                    int minutes;
                    if (arr.length == 2) {
                        minutes = Integer.parseInt(arr[0]) * 60 + Integer.parseInt(arr[1]);
                    } else {
                        minutes = Integer.parseInt(arr[0]) * 60;
                    }

                    activity.getProfile().setGameTimer(true);
                    activity.getProfile().setGameTimerInMinutes(minutes);
                    activity.getGame().setInitialTimer(minutes);
                } else {
                    etTimer.setVisibility(View.GONE);
                    activity.getProfile().setGameTimer(false);
                    activity.getProfile().setGameTimerInMinutes(0);

                }
            }
        });

        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                activity.finish();
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (tbTimer.isChecked()) {
                    activity.startTimer(etTimer.getText().toString());
                }
            }
        });


    }
}
