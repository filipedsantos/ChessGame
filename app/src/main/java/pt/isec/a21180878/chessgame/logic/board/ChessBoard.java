package pt.isec.a21180878.chessgame.logic.board;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import pt.isec.a21180878.chessgame.logic.pieces.Piece;

/**
 * Created by Filipe on 29-Nov-17.
 */

public class ChessBoard implements Serializable {
    private static final int BOARDSIZE = 8;
    private HashMap<Position, Piece> chessBoard;

    public ChessBoard() {
        this.chessBoard = new HashMap<Position, Piece>(32);
    }

    public boolean isOccupied(Position pos) {
        return chessBoard.containsKey(pos);
    }

    public Piece getPiece(Position position) {
        return chessBoard.get(position);
    }

    public Piece getPiece(String position) {
        return chessBoard.get(new Position(position));
    }

    /**
     * @return A set containing all of the mappings in the chessboard.
     */
    public Set<Map.Entry<Position, Piece>> getEntrySet() {
        return chessBoard.entrySet();
    }

    public Piece putPiece(Position pos, Piece p) {
        if(p == null) {
            throw new IllegalArgumentException("[ERROR] IllegalArgumentException -- Piece is null!");
        }
        if(p.getPosition() != null && !p.getPosition().equals(pos)) {
            Position position = p.getPosition();
            chessBoard.remove(position);
        }

        Piece piece = chessBoard.get(pos);
        p.setPosition(pos);
        if(piece != null) {
            piece.setPosition(null);
        }
        chessBoard.put(pos, p);

        return piece;
    }

    public boolean isValidPosition(Position pos) {
        return (pos.getLine() >= 0 && pos.getLine() < 8) &&
                (pos.getColumn() >= 0 && pos.getColumn() < 8);
    }

    public void remove(Position pos) {
        chessBoard.remove(pos);
    }
}
