package pt.isec.a21180878.chessgame.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import pt.isec.a21180878.chessgame.logic.Move;

/**
 * Created by Filipe on 05-Jan-18.
 */

public class GameLog implements Serializable{

    private static final long serialVersionUID = 42L;

    private int gameMode;
    private String player1;
    private String player2;
    private Date date;
    private boolean win;
    private ArrayList<Move> moves;

    public GameLog(int gameMode, String player1, String player2, Date date, ArrayList<Move> moveSequence, boolean win) {
        this.gameMode = gameMode;
        this.player1 = player1;
        this.player2 = player2;
        this.date = date;
        this.win = win;
        this.moves = moveSequence;
    }

    public String getPlayer1() {
        return player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }

    public boolean getWin() {
        return win;
    }

    public int getGameMode() {
        return gameMode;
    }
}
