package pt.isec.a21180878.chessgame.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import pt.isec.a21180878.chessgame.R;
import pt.isec.a21180878.chessgame.activities.ChessGameActivity;

/**
 * Created by Filipe on 15/12/2017.
 */

public class NewGameDialog extends Dialog implements View.OnClickListener, Constants {
    private Activity activity;
    private Button singleplayer, mutiplayerOffline, multiplayerOnline;

    public NewGameDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_game);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        singleplayer = (Button) findViewById(R.id.bt_singleplayer);
        mutiplayerOffline = (Button) findViewById(R.id.bt_multiplayer_offline);
        multiplayerOnline = (Button) findViewById(R.id.bt_multiplayer_online);

        singleplayer.setOnClickListener(this);
        mutiplayerOffline.setOnClickListener(this);
        multiplayerOnline.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.bt_singleplayer:
                intent = new Intent(activity, ChessGameActivity.class);
                intent.putExtra(GAMEMODE, SINGLEPLAYER);
                activity.startActivity(intent);
                this.dismiss();
                break;
            case R.id.bt_multiplayer_offline:
                intent = new Intent(activity, ChessGameActivity.class);
                intent.putExtra(GAMEMODE, MULTIPLAYER_OFFLINE);
                activity.startActivity(intent);
                this.dismiss();
                break;
            case R.id.bt_multiplayer_online:
                intent = new Intent(activity, ChessGameActivity.class);
                intent.putExtra(GAMEMODE, MULTIPLAYER_ONLINE);
                activity.startActivity(intent);
                this.dismiss();
                break;
            default:
                break;
        }
    }
}
