package pt.isec.a21180878.chessgame.logic.pieces;

import java.io.Serializable;
import java.util.ArrayList;

import pt.isec.a21180878.chessgame.logic.board.Position;
import pt.isec.a21180878.chessgame.logic.Game;

/**
 * Created by Filipe on 22-Nov-17.
 */

public class Bishop extends Piece implements Serializable {

    public Bishop(boolean isWhite, Game game, Position pos) {
        super(isWhite, game, pos);
    }

    @Override
    public char getPieceShortName() {
        return isWhite() ? 'B' : 'b';
    }

    @Override
    public ArrayList<Position> getAttackedPositions() {
        ArrayList<Position> positions = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            int dir = 45 + 90 * i;
            Position p = getPosition().getAdjacentPosition(dir);
            while (getChessBoard().isValidPosition(p)) {
                positions.add(p);
                if(getChessBoard().isOccupied(p))
                    break;
                p = p.getAdjacentPosition(dir);
            }
        }
        return positions;
    }

    @Override
    public ArrayList<Position> getMovePositions() {
        ArrayList<Position> positions = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            int direction = 45 + 90 * i;
            Position p = getPosition().getAdjacentPosition(direction);
            while (getChessBoard().isValidPosition(p)) {
                if(getChessBoard().isOccupied(p)){
                    if(!isSameColor(getChessBoard().getPiece(p)))
                        positions.add(p);
                    break;
                }
                positions.add(p);
                p = p.getAdjacentPosition(direction);
            }
        }
        return positions;
    }

    @Override
    public int getValue() {
        return 30;
    }

}
