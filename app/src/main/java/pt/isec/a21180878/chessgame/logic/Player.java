package pt.isec.a21180878.chessgame.logic;

import java.io.Serializable;

import pt.isec.a21180878.chessgame.utils.Profile;

/**
 * Created by Filipe on 26-Dec-17.
 */

public class Player implements Serializable {

    private Profile profile;

    private int playerType;
    private int onlinePlayerType = -1;

    public Player() {
        this.profile = new Profile();
    }

    public Profile getProfile() {
        return profile;
    }

    public void setOnlinePlayerType(int type) {
        this.onlinePlayerType = type;
    }

    public int getOnlinePlayerType() {
        return onlinePlayerType;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public int getPlayerType() {
        return playerType;
    }

    public void setPlayerType(int playerType) {
        this.playerType = playerType;
    }
}
